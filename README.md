# Bizee challenge

- This repository contains the code for the Bizee coding challenge

# How to run

- To run this project run

npm install

npm run dev

App is available in

http://localhost:5173/

## NOTES

On the /stock call, there is one product missing with id 5
