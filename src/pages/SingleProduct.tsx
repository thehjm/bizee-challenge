import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import getProducts from "../api/getProducts";
import getStock from "../api/getStock";
import getPrices from "../api/getPrices";
import buildProductSchema from "../utils/product-utils";
import Paper from "@mui/material/Paper";
import ButtonGroup from "@mui/material/ButtonGroup";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import ToggleButton from "@mui/material/ToggleButton";
import ToggleButtonGroup from "@mui/material/ToggleButtonGroup";
import { useParams } from "react-router-dom";
import { Product } from "../api/getProducts";
import { useEffect, useState } from "react";
import { CartItem } from "../context/ShoppingContext";
import { useShoppingCartContext } from "../context/useShoppingCartContext";

const SingleProduct = () => {
  const { id } = useParams<{ id: string }>();
  const [product, setProduct] = useState<Product | null>(null);
  const [productForm, setProductForm] = useState({
    count: 1,
    size: product?.size || "S",
  });
  const [error, setError] = useState<string | null>(null);
  const { setCart } = useShoppingCartContext();

  useEffect(() => {
    if (!id) {
      alert("No product id found");
      return;
    }

    const fetchData = async () => {
      const products = await getProducts();
      const stock = await getStock();
      const prices = await getPrices();

      const fullProducts = buildProductSchema({ products, stock, prices });

      const foundProduct = fullProducts.find(
        (product) => product.id === parseInt(id, 10)
      );

      if (foundProduct) {
        setProduct(foundProduct);
      } else {
        setError("Product not found");
      }
    };
    fetchData();
  }, [id]);

  if (error) {
    return <div>{error}</div>;
  }

  const reduceQuantity = () => {
    if (productForm.count > 0) {
      setProductForm((prev) => ({ ...prev, count: prev.count - 1 }));
    }
  };

  const increaseQuantity = () => {
    if (product) {
      if (productForm.count < product?.stock) {
        setProductForm((prev) => ({ ...prev, count: prev.count + 1 }));
      }
    }
  };

  const handleSizeChange = (
    event: React.MouseEvent<HTMLElement>,
    newSize: string
  ) => {
    setProductForm((prev) => ({ ...prev, size: newSize }));
  };

  const handleAddToCart = () => {
    if (product) {
      const productToAdd: CartItem = {
        ...product,
        size: productForm.size,
        quantity: productForm.count,
      };
      setCart((prevCart: Product[]) => [...prevCart, productToAdd]);
    }
  };

  return (
    <Container>
      <Paper elevation={2}>
        <Box display="flex" gap={2}>
          <Box>
            <img src={product?.image} alt={product?.model} width={400} />
          </Box>
          <Box padding={4} display="flex" flexDirection="column" gap={2}>
            <Typography variant="h4">{product?.model}</Typography>
            <Typography variant="body1">{product?.price}</Typography>
            <Box display="flex" gap={4}>
              <Typography variant="h5">Size: </Typography>
              <ToggleButtonGroup
                value={productForm.size}
                size="large"
                exclusive
                onChange={handleSizeChange}
                aria-label="Basic button group"
              >
                <ToggleButton value="S">S</ToggleButton>
                <ToggleButton value="M">M</ToggleButton>
                <ToggleButton value="L">L</ToggleButton>
              </ToggleButtonGroup>
            </Box>
            <Box display="flex" gap={4}>
              <Typography variant="h5">QTY: </Typography>
              <ButtonGroup
                variant="outlined"
                size="large"
                aria-label="Basic button group"
              >
                <Button onClick={reduceQuantity}>-</Button>
                <Button>{productForm.count}</Button>
                <Button onClick={increaseQuantity}>+</Button>
              </ButtonGroup>
            </Box>
            <Box marginX={0} marginY={4} onClick={handleAddToCart}>
              <Button variant="contained" color="primary" sx={{ right: 0 }}>
                Add to cart
              </Button>
            </Box>
          </Box>
        </Box>
      </Paper>
    </Container>
  );
};

export default SingleProduct;
