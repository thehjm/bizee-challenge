import Container from "@mui/material/Container";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import { useShoppingCartContext } from "../context/useShoppingCartContext";
import Paper from "@mui/material/Paper";
import { CartItem } from "../context/ShoppingContext";
import CartRow from "../components/CartRow";

const ShoppingCart = () => {
  const { cart } = useShoppingCartContext();

  return (
    <Container>
      <Box width="100%">
        <Typography variant="h4"> Shopping Cart</Typography>
        <Box mt={2}>
          {cart.length === 0 && (
            <Typography variant="h6">Your cart is empty</Typography>
          )}
          <Paper>
            <Box padding={4}>
              <Box>
                {cart.length > 0 &&
                  cart.map((product: CartItem) => (
                    <CartRow key={product.id} product={product} />
                  ))}
              </Box>
              <Box>
                <Typography variant="h5" align="right">
                  Subtotal: ${" "}
                  {cart
                    .reduce(
                      (acc: number, item: CartItem) =>
                        acc + item.price * (item.quantity || 1),
                      0
                    )
                    .toFixed(2)}
                </Typography>
              </Box>
            </Box>
          </Paper>
        </Box>
      </Box>
    </Container>
  );
};

export default ShoppingCart;
