import React, { useEffect } from "react";
import Grid from "@mui/material/Grid";
import getProducts, { Product } from "../api/getProducts";
import getStock from "../api/getStock";
import getPrices from "../api/getPrices";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardActions from "@mui/material/CardActions";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import buildProductSchema from "../utils/product-utils";
import { useShoppingCartContext } from "../context/useShoppingCartContext";
import { useNavigate } from "react-router-dom";

const ProductList = () => {
  const [products, setProducts] = React.useState<Product[]>([]);
  const { setCart } = useShoppingCartContext();
  const navigate = useNavigate();

  useEffect(() => {
    const fetchData = async () => {
      const products = await getProducts();
      const stock = await getStock();
      const prices = await getPrices();

      const fullProducts = buildProductSchema({ products, stock, prices });

      setProducts(fullProducts);
    };
    fetchData();
  }, []);

  const addToCart = (product: Product) => {
    setCart((prevCart: Product[]) => [...prevCart, product]);
  };

  return (
    <Grid container spacing={2}>
      {products.length > 0 &&
        products.map((product: Product) => {
          return (
            product.stock > 0 && (
              <Grid key={product.id} item md={4} sm={6} xs={12}>
                <Card
                  sx={{
                    minHeight: 440,
                    height: "100%",
                    display: "flex",
                    flexDirection: "column",
                  }}
                  onClick={() => navigate(`/product/${product.id}`)}
                >
                  <CardMedia
                    component="img"
                    sx={{ height: 180 }}
                    image={product.image}
                  />
                  <CardContent>
                    <Typography variant="h2">{product.model}</Typography>
                    <Typography variant="body1">{product.price}</Typography>
                  </CardContent>
                  <CardActions sx={{ mt: "auto" }}>
                    <Button onClick={() => addToCart(product)}>
                      Add to cart
                    </Button>
                  </CardActions>
                </Card>
              </Grid>
            )
          );
        })}
    </Grid>
  );
};

export default ProductList;
