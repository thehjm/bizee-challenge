enum ImageType {
  BUTTON_DOWN_SHIRT = "button-down shirt",
  POLO_SHIRT = "polo shirt",
  HOODIE = "hoodie",
  T_SHIRT = "t-shirt",
}

import ButtonDownShirt from "../assets/button-down-shirt.jpg";
import PoloShirt from "../assets/polo-shirt.jpg";
import Hoodie from "../assets/hoodie.jpg";
import TShirt from "../assets/t-shirt.jpg";

export const getProductImage = (model: string): string => {
  if (model === ImageType["BUTTON_DOWN_SHIRT"]) {
    return ButtonDownShirt;
  } else if (model === ImageType["POLO_SHIRT"]) {
    return PoloShirt;
  } else if (model === ImageType["HOODIE"]) {
    return Hoodie;
  } else if (model === ImageType["T_SHIRT"]) {
    return TShirt;
  } else {
    return "";
  }
};
