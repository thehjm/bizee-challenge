import { Price } from "../api/getPrices";
import { Product } from "../api/getProducts";
import { Stock } from "../api/getStock";
import { getProductImage } from "./get-image";

const buildProductSchema = ({
  products,
  stock,
  prices,
}: {
  products: Product[];
  stock: Stock[];
  prices: Price[];
}) => {
  return products.map((product) => {
    const stockItem = stock.find((item) => item.product_id === product.id);
    const priceItem = prices.find((item) => item.product_id === product.id);

    return {
      id: product.id,
      model: product.model,
      code: product.code,
      size: product.size,
      stock: stockItem?.stock || 0,
      price: priceItem?.price || 0,
      image: getProductImage(product.model),
    };
  });
};

export default buildProductSchema;
