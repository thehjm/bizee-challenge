import { useShoppingCartContext } from "../context/useShoppingCartContext";
import { CartItem } from "../context/ShoppingContext";
import Box from "@mui/material/Box";
import Typography from "@mui/material/Typography";
import IconButton from "@mui/material/IconButton";
import ClearIcon from "@mui/icons-material/Clear";

const CartRow = ({ product }: { product: CartItem }) => {
  const { setCart } = useShoppingCartContext();

  const handleRemoveItem = (id: number) => {
    setCart((prevCart: CartItem[]) =>
      prevCart.filter((item) => item.id !== id)
    );
  };

  return (
    <Box key={product.id} display="flex" my={2}>
      <img src={product.image} alt={product.model} width={120} height={120} />
      <Box ml={2} flexGrow={1}>
        <Typography variant="h4">{product.model}</Typography>
        <Typography variant="body1">
          Quantity: {product.quantity || 1}
        </Typography>
        <Typography variant="body1">{product.size}</Typography>
        <Typography variant="body1">$ {product.price}</Typography>
      </Box>

      <Box>
        <IconButton onClick={() => handleRemoveItem(product.id)}>
          <ClearIcon />
        </IconButton>
      </Box>
    </Box>
  );
};

export default CartRow;
