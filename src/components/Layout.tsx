import Box from "@mui/material/Box";
import Container from "@mui/material/Container";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import ShoppingCartIcon from "@mui/icons-material/ShoppingCart";
import Badge from "@mui/material/Badge";
import { ReactNode } from "react";
import { useShoppingCartContext } from "../context/useShoppingCartContext";
import { Typography } from "@mui/material";
import { useNavigate } from "react-router-dom";

const Layout = ({ children }: { children: ReactNode }) => {
  const { cart } = useShoppingCartContext();
  const navigate = useNavigate();

  return (
    <Box width="100vw" height="100vh">
      <AppBar position="static">
        <Container maxWidth="lg">
          <Toolbar>
            <Box
              id="toolbar-container"
              width="100%"
              onClick={() => navigate("/", { replace: true })}
            >
              <Typography variant="h6">Bizee challenge</Typography>
            </Box>
            <Box>
              <IconButton
                onClick={() => navigate("/shopping-cart", { replace: true })}
              >
                <Badge badgeContent={cart.length} color="info">
                  <ShoppingCartIcon />
                </Badge>
              </IconButton>
            </Box>
          </Toolbar>
        </Container>
      </AppBar>
      <Container>
        <Box mt={4}>{children}</Box>
      </Container>
    </Box>
  );
};

export default Layout;
