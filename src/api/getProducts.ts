export type Product = {
  id: number;
  model: string;
  code: string;
  size: string;
  stock: number;
  price: number;
  image: string;
};

const getProducts = async (): Promise<Product[]> => {
  try {
    const response = await fetch("http://localhost:3000/products");
    return response.json();
  } catch (error) {
    console.log(error);
    throw new Error("Error fetching products");
  }
};

export default getProducts;
