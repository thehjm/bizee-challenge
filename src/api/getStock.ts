export type Stock = {
  id: number;
  product_id: number;
  stock: number;
};

const getStock = async (): Promise<Stock[]> => {
  try {
    const response = await fetch("http://localhost:3000/stock");
    return response.json();
  } catch (error) {
    console.log(error);
    throw new Error("Error fetching products");
  }
};

export default getStock;
