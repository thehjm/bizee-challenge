export type Price = {
  id: number;
  product_id: number;
  price: number;
};

const getPrices = async (): Promise<Price[]> => {
  try {
    const response = await fetch("http://localhost:3000/prices");
    return response.json();
  } catch (error) {
    console.log(error);
    throw new Error("Error fetching products");
  }
};

export default getPrices;
