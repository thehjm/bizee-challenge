import { useContext } from "react";
import {
  ShoppingCartContext,
  ShoppingCartContextType,
} from "./ShoppingContext";

export const useShoppingCartContext = (): ShoppingCartContextType => {
  const context = useContext(ShoppingCartContext);
  if (!context) {
    throw new Error(
      "useShoppingCartContext must be used within a ShoppingCartProvider"
    );
  }
  return context;
};
