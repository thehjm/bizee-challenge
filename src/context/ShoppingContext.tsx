import React, { createContext, useState, ReactNode } from "react";
import { Product } from "../api/getProducts";

export interface CartItem extends Product {
  quantity: number;
}

type ShoppingCartContextType = {
  cart: CartItem[];
  setCart: React.Dispatch<React.SetStateAction<CartItem[]>>;
};

export const ShoppingCartContext =
  createContext<ShoppingCartContextType | null>(null);

export interface ShoppingCartProviderProps {
  children: ReactNode;
}

export const ShoppingCartProvider: React.FC<ShoppingCartProviderProps> = ({
  children,
}) => {
  const [cart, setCart] = useState<CartItem[]>([]);

  return (
    <ShoppingCartContext.Provider value={{ cart, setCart }}>
      {children}
    </ShoppingCartContext.Provider>
  );
};
